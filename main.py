#!usr/bin/env python
# -*- coding: utf-8 -*-

# Paycheck Calculator
"""This program is a take-home pay calculator for hourly workers."""

hours_worked = input('Enter how many hours you\'ve worked this pay period:  ')
tax_percentage = input('Enter your total tax percentage:  ')
tax_percentage = float(tax_percentage) / 100
hourly_wage = input('What is your hourly wage?:  ')


def main():
    """This function will decide how many and how much deductions are."""
    _deductions = input('How many deductions do you have? (up to three):  ')
    if _deductions == 'one':
        single_deduction_inputs()
        math()
        post_tax_deduct()
    elif _deductions == 'two':
        double_deduction_inputs()
        math_2()
        post_tax_deduct()
    elif _deductions == 'three':
        triple_deduction_inputs()
        math_3()
        post_tax_deduct()
    else:
        print('You have no deductions.')


def single_deduction_inputs():
    global amount
    single_deduct = input('What is your deduction named?:   ')
    amount = input('What is the percentage?:   ')
    print('Your single deduction ' + single_deduct + ' is ' + amount + " percent.")
    amount = float(amount) / 100
    print(amount)


def double_deduction_inputs():
    """This function works out having 2 deductions."""
    global amount1, amount2
    first_deduct = input('What is your first deduction named?:   ')
    amount1 = input('What is the percentage?:   ')
    print('Your first deduction ' + first_deduct + ' is ' + amount1 + " percent.")
    amount1 = float(amount1) / 100
    second_deduct = input('What is your second deduction named?:   ')
    amount2 = input('What is the percentage?:   ')
    print('Your second deduction ' + second_deduct + ' is ' + amount2 + " percent.")
    amount2 = float(amount2) / 100


def triple_deduction_inputs():
    """This works out having three deductions."""
    global amount1, amount2, amount3
    first_deduct = input('What is your first deduction named?  :')
    amount1 = input('What is the percentage?:   ')
    print('Your first deduction ' + first_deduct + ' is ' + amount1 + " percent.")
    amount1 = float(amount1) / 100
    second_deduct = input('What is your second deduction named?  :')
    amount2 = input('What is the percentage?:   ')
    print('Your second deduction ' + second_deduct + ' is ' + amount2 + " percent.")
    amount2 = float(amount2) / 100
    third_deduct = input('What is your third deduction named?    :')
    amount3 = input('What is the percentage?:   ')
    print('Your third deduction ' + second_deduct + first_deduct + third_deduct + ' is ' + amount3 + " percent.")
    amount3 = float(amount3) / 100


def math():
    """
    This main function will take the number of hours,
    and multiply it by the hourly rate.
    We will then apply the pre-tax deductions,
    and then the tax to get the take home amount.
    """
    global pretax
    gross_pay = float(hours_worked) * float(hourly_wage)
    deductamt = gross_pay * amount
    prededuct = gross_pay - deductamt
    taxamt = prededuct * tax_percentage
    pretax = prededuct - taxamt
    print("Your pre tax amount is:   "
          "\n")
    print(pretax)


def math_2():
    """This function is for 2 deductions."""
    global pretax
    gross_pay = float(hours_worked) * float(hourly_wage)
    deductadd = float(amount1) + float(amount2)
    deductamt = gross_pay * deductadd
    prededuct = gross_pay - deductamt
    taxamt = gross_pay * tax_percentage
    pretax = prededuct - taxamt
    print("Your pre tax amount is:   "
          "\n")
    print(pretax)


def math_3():
    """This function is for 3 deductions."""
    global pretax
    gross_pay = float(hours_worked) * float(hourly_wage)
    deductadd = float(amount1) + float(amount2) + float(amount3)
    deductamt = gross_pay * deductadd
    prededuct = gross_pay - deductamt
    taxamt = prededuct * tax_percentage
    pretax = prededuct - taxamt
    print("Your pre tax amount is:   "
          "\n")
    print(pretax)


def post_tax_deduct():
    """This is for anything else that comes out of your pay POST-tax."""
    ae = input("Is there any post-tax deductions that should be deducted from your pay?:  ")
    if ae == "no":
        print("Your take home pay remains the same.")
    else:
        ae_what = input("What is your post-tax deduction?:   ")
        ae_amount = input("How much is " + ae_what + "?:  ")
        final = float(pretax) - float(ae_amount)
        print("Your take home will be ")
        print(final)
# How to do 'if user picked 1 or 2 deductions?' also diff between fixed v Perc


if __name__ == '__main__':
    main()

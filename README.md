# README #


### What is this repository for? ###

* This is a calculator for hourly workers to see what their take-home pay will be after inputting deductions and taxes
* Version 1.0

### How do I get set up? ###

* You'll be asked your hourly wage, tax amount, and how many hours per week.
* When asked how many deductions, you must enter the word 'one', 'two', or 'three'

### Who do I talk to? ###

* russelcorbo@gmail.com